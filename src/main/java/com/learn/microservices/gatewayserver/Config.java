package com.learn.microservices.gatewayserver;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository;
import org.springframework.security.web.server.SecurityWebFilterChain;

@EnableWebFluxSecurity
public class Config {

    @Bean
    public SecurityWebFilterChain securitygWebFilterChain(ServerHttpSecurity http, ReactiveClientRegistrationRepository clientRegistrationRepository) {
        http.oauth2Login();
        http.csrf().disable();
        http.cors().disable();
        return
            http.authorizeExchange()
                .anyExchange()
                .permitAll()
                .and()
                .build();
    }

}
